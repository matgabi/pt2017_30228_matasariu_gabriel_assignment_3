package dbAccess;
import java.util.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Customer;
import dbConnection.ConnectionFactory;

public class DBCustomers {
	private static final String getCustomersString = "select * from customer";
	private static final String deleteCustomerString = "delete from customer where customerId = ?";
	private static final String addNewCustomerString = "insert into customer values(?,?,?,?)";
	private static final String updateByIdString = "update customer set name = ?, address = ? , email = ? where customerId = ?";
	
	public static List<Customer> getCustomers(){
		List<Customer> customers = new ArrayList<Customer>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement getCustomers = null;
		ResultSet rs = null;
		try{
			getCustomers = dbConnection.prepareStatement(getCustomersString);
			rs = getCustomers.executeQuery();
			while(rs.next()){
				int id = rs.getInt("customerId");
				String name = rs.getString("name");
				String address = rs.getString("address");
				String email = rs.getString("email");
				customers.add(new Customer(id,name,address,email));	
			}
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(getCustomers);
			ConnectionFactory.close(dbConnection);
		}
		return customers;
	}
	
	public static void deleteCustomer(int customerId){
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteCustomer = null;
		int rs;
		try{
			deleteCustomer = dbConnection.prepareStatement(deleteCustomerString);
			deleteCustomer.setInt(1, customerId);
			rs = deleteCustomer.executeUpdate();
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(deleteCustomer);
			ConnectionFactory.close(dbConnection);
		}
		
	}
	
	public static void addNewCustomer(int customerId,String name,String address,String email){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement addNewCustomer = null;
		try{
			addNewCustomer = dbConnection.prepareStatement(addNewCustomerString);
			addNewCustomer.setInt(1, customerId);
			addNewCustomer.setString(2, name);
			addNewCustomer.setString(3, address);
			addNewCustomer.setString(4, email);
			addNewCustomer.executeUpdate();
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(addNewCustomer);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static void updateById(int idCustomer,String name,String address,String email){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateCustomer = null;
		try{
			updateCustomer = dbConnection.prepareStatement(updateByIdString);
			updateCustomer.setInt(4, idCustomer);
			updateCustomer.setString(1, name);
			updateCustomer.setString(2, address);
			updateCustomer.setString(3, email);
			updateCustomer.executeUpdate();
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(updateCustomer);
			ConnectionFactory.close(dbConnection);
		}
	}
}

package model;

public class Payment {

	private int customerId;
	private String customerName;
	private int total;
	
	public Payment(int id,String name,int total){
		this.customerId = id;
		this.customerName = name;
		this.total = total;
	}
	
	public void setCId(int id){
		this.customerId = id;
	}
	
	public void setCName(String name){
		this.customerName = name;
	}
	
	public void setTotal(int total){
		this.total = total;
	}
	
	public int getCId(){
		return this.customerId;
	}
	
	public String getCName(){
		return this.customerName;
	}
	
	public int getTotal(){
		return this.total;
	}
	
	public String toString(){
		return this.customerId + " " + this.customerName + " " + this.total;
	}
}

package dbAccess;

import java.util.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Order;
import dbConnection.ConnectionFactory;

public class DBOrders {

	private static final String getOrdersString = "select o.orderId,o.customerId,c.name as namec,o.productId,p.name as namep,o.quantity from orders o natural join customer c join  products p on(o.productId = p.productId)";
	private static final String getOrdersByClientIdString = "select o.orderId,o.customerId,c.name as namec,o.productId,p.name as namep,o.quantity from orders o "
			+ "													natural join customer c join  products p on(o.productId = p.productId) where customerId=?";
	private static final String addNewOrderString = "insert into orders values(?,?,?,?)";
	private static final String deleteOrderString = "delete from orders where orderId = ?";
	
	public static List<Order> getOrders(){
		List<Order> orders = new ArrayList<Order>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement getOrders = null;
		ResultSet rs = null;
		try{
			getOrders = dbConnection.prepareStatement(getOrdersString);
			rs = getOrders.executeQuery();
			while(rs.next()){
				int orderId = rs.getInt("orderId");
				int customerId = rs.getInt("customerId");
				String nameCustomer = rs.getString("namec");
				int productId = rs.getInt("productId");
				String productName = rs.getString("namep");
				int quantity = rs.getInt("quantity");
				orders.add(new Order(orderId,customerId,nameCustomer,productId,productName,quantity));
			}
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(getOrders);
			ConnectionFactory.close(dbConnection);
		}
		return orders;
	}
	
	public static List<Order> getOrdersByClientId(int clientId){
		List<Order> orders = new ArrayList<Order>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement getOrdersByClientId = null;
		ResultSet rs = null;
		try{
			getOrdersByClientId = dbConnection.prepareStatement(getOrdersByClientIdString);
			getOrdersByClientId.setInt(1, clientId);
			rs = getOrdersByClientId.executeQuery();
			while(rs.next()){
				int orderId = rs.getInt("orderId");
				int customerId = rs.getInt("customerId");
				String nameCustomer = rs.getString("namec");
				int productId = rs.getInt("productId");
				String productName = rs.getString("namep");
				int quantity = rs.getInt("quantity");
				orders.add(new Order(orderId,customerId,nameCustomer,productId,productName,quantity));
			}
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(getOrdersByClientId);
			ConnectionFactory.close(dbConnection);
		}
		return orders;
	}

	public static void insertOrder(int orderId,int customerId,int productId,int quantity){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement addNewOrder = null;
		try{
			addNewOrder = dbConnection.prepareStatement(addNewOrderString);
			addNewOrder.setInt(1, orderId);
			addNewOrder.setInt(2, customerId);
			addNewOrder.setInt(3, productId);
			addNewOrder.setInt(4, quantity);
			addNewOrder.executeUpdate();
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(addNewOrder);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static void deleteOrder(int orderId){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteOrder = null;
		try{
			deleteOrder = dbConnection.prepareStatement(deleteOrderString);
			deleteOrder.setInt(1, orderId);
			deleteOrder.executeUpdate();
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(deleteOrder);
			ConnectionFactory.close(dbConnection);
		}
	}

}

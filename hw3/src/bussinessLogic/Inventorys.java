package bussinessLogic;

import java.util.*;
import model.Inventory;
import dbAccess.DBInventory;

public class Inventorys {
	
	public static List<Inventory> getInventory(){
		return DBInventory.getInventory();
	}
    
	public static int getQuantityById(int productId){
		return DBInventory.getQuantityById(productId);
	}
}

package model;

public class Product {

	private int productId;
	private String name;
	private String description;
	private int price;
	
	public Product(int productId,String name,String description,int price){
		this.productId = productId;
		this.name = name;
		this.description = description;
		this.price = price;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setId(int id){
		this.productId = id;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
	
	public void setPrice(int price){
		this.price = price;
	}
	
	public int getId(){
		return this.productId;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getDescription(){
		return this.description;
	}
	
	public int getPrice(){
		return this.price;
	}
	
	public String toString(){
		return this.productId + " " + this.name + " " + this.description + " " + this.price;
	}
	
}

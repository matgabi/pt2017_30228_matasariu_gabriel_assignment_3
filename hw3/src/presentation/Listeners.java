package presentation;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;
import java.util.List;
import javax.swing.event.*;

import bill.Writer;

import javax.swing.*;
import java.awt.*;

import bussinessLogic.*;
import dbAccess.NrOrders;
import model.*;
import reflection.ReflectionTable;

public class Listeners {
	
	private JPanel view;
	private JPanel insert = new JPanel();
	private JPanel showMessages = new JPanel();
	
	public Listeners(JPanel view){
		this.view = view;
		CustomerView.setInfo(view, showMessages, insert);
	}

	public ActionListener getCustomerListener(){
		return new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				CustomerView.showCustomers();
			}
		};
	}
	
	public ActionListener getProductListener(){
		return new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				view.setLayout(new BoxLayout(view, BoxLayout.Y_AXIS));
				java.util.List<Product> products = Products.getProducts();
				JTable table;
				table = ReflectionTable.retrieveTable(products);
				addProductListener(table,products);
				view.removeAll();
				if(table != null){
					view.add(table);
					view.setAlignmentX(Component.LEFT_ALIGNMENT);
				}
				else 
					view.add(new JLabel("Table is empty!"));
				showMessages.setAlignmentX(Component.CENTER_ALIGNMENT);
				insert.removeAll();
				JTextField id = new JTextField(6);
				JTextField name = new JTextField(20);
				JTextField description = new JTextField(30);
				JTextField price = new JTextField(6);
				JButton addProduct = new JButton("Insert");
				insert.add(new JLabel("Product ID :"));
				insert.add(id);
				insert.add(new JLabel("Name :"));
				insert.add(name);
				insert.add(new JLabel("Description :"));
				insert.add(description);
				insert.add(new JLabel("Price :"));
				insert.add(price);
				insert.add(addProduct);
				showMessages.removeAll();
				addProduct.addActionListener(getInsertProdListener(id,name,description,price));
				view.add(insert);
				view.add(showMessages);
				view.revalidate();
				view.repaint();			
			}
		};
	}
	
	public ActionListener getInventoryListener(){
		return new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				java.util.List<Inventory> inventory = Inventorys.getInventory();
				JTable table;
				table = ReflectionTable.retrieveTable(inventory);
				view.removeAll();
				if(table != null)
					view.add(table);
				else 
					view.add(new JLabel("Table is empty!"));
				view.revalidate();
				view.repaint();
				
				
			}
		};
	}
	
	public ActionListener getOrdersListener(){
		return new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				view.setLayout(new BoxLayout(view, BoxLayout.Y_AXIS));
				java.util.List<Order> orders = Orders.getOrders();
				java.util.List<Customer> cust= Customers.getCustomers();
				java.util.List<Product> prod = Products.getProducts();
				String[] customers = new String[cust.size()];
				String[] products = new String[prod.size()];
				int i = 0;
				for(Customer c : cust){
					customers[i++] = c.getId() +"." + c.getName();
				}
				i=0;
				for(Product p : prod){
					products[i++] = p.getId() + "." + p.getName();
				}
				JTable table;
				table = ReflectionTable.retrieveTable(orders);
				view.removeAll();
				if(table != null)
					view.add(table);
				else 
					view.add(new JLabel("Table is empty!"));
				insert.removeAll();
				JComboBox customerName = new JComboBox(customers);
				JComboBox productName = new JComboBox(products);
				JTextField quantity = new JTextField(6);
				
				JButton addOrder = new JButton("Make new order");
				insert.add(new JLabel("Customer Name :"));
				insert.add(customerName);
				insert.add(new JLabel(" Product Name :"));
				insert.add(productName);
				insert.add(new JLabel("Quantity :"));
				insert.add(quantity);
				quantity.setText("1");
				addOrder.addActionListener(getMakeOrderListener(customerName,productName,quantity));
				insert.add(addOrder);
				showMessages.removeAll();
				
				addOrderListener(table,orders);
				
				view.add(insert);
				view.add(showMessages);
				view.revalidate();
				view.repaint();
				
				
			}
		};
	}
	
	public ActionListener getPaymentsListener(){
		return new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				java.util.List<Payment> payments = Payments.getPayments();
				JTable table;
				table = ReflectionTable.retrieveTable(payments);
				view.removeAll();
				if(table != null)
					view.add(table);
				else 
					view.add(new JLabel("Table is empty!"));
				view.revalidate();
				view.repaint();
				
				
			}
		};
	}
	
	
	
	private void addProductListener(JTable table,List<Product> products){
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
		    @Override
		    public void valueChanged(ListSelectionEvent event) {
		    	
		        if (table.getSelectedRow() > 0) {
		           int productId = new Integer(table.getValueAt(table.getSelectedRow(), 0).toString());
		           Product toBeShowed = null;
		           for(Product p : products){
		        	   if(p.getId() == productId){
		        		   toBeShowed = p;
		        		   break;
		        	   }
		           }
		           showMessages.setAlignmentX(Component.LEFT_ALIGNMENT);
		           view.removeAll();
		           view.setLayout(new BoxLayout(view, BoxLayout.Y_AXIS));
		           view.add(new JLabel("Product Id : " + toBeShowed.getId()+""));
		           view.add(new JLabel("Name : " + toBeShowed.getName()));
		           view.add(new JLabel("Description : " + toBeShowed.getDescription()));
		           view.add(new JLabel("Price : " + toBeShowed.getPrice()));
		           view.add(new JLabel(" "));
		           view.add(new JLabel(" "));
		           view.add(new JLabel("Items available: " + Inventorys.getQuantityById(toBeShowed.getId()) ));
		           view.add(new JLabel(" "));
		           view.add(new JLabel(" "));
		           
		           JTextField newName = new JTextField(20);
		           JTextField newDescription = new JTextField(20);
		           JTextField newPrice = new JTextField(20);
		           JButton updateInfo = new JButton("Update info");
		           newName.setText(toBeShowed.getName());
		           newDescription.setText(toBeShowed.getDescription());
		           newPrice.setText(toBeShowed.getPrice()+"");
		           
		           JPanel update = new JPanel();
		           update.add(new JLabel("New product name: "));
		           update.add(newName);
		           update.add(new JLabel("New description: "));
		           update.add(newDescription);
		           update.add(new JLabel("New price: "));
		           update.add(newPrice);
		           update.add(updateInfo);
		           updateInfo.addActionListener(getUpdateProductListener(productId,newName,newDescription,newPrice));
		           update.setAlignmentX(Component.LEFT_ALIGNMENT);
		           JButton delete = new JButton("Delete product");
		           delete.addActionListener(getDeleteProductListener(productId));
		           
		           showMessages.removeAll();
		           view.add(update);
		           view.add(showMessages);
		           view.add(delete);
		           view.revalidate();
		           view.repaint();
		        }
		    }
		});
	}
	
	private void addOrderListener(JTable table,List<Order> orders){
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
		    @Override
		    public void valueChanged(ListSelectionEvent event) {
		        if (table.getSelectedRow() > 0) {
		           int orderId = new Integer(table.getValueAt(table.getSelectedRow(), 0).toString());
		           Order toBeShowed = null;
		           for(Order o : orders){
		        	   if(o.getOId() == orderId){
		        		   toBeShowed = o;
		        		   break;
		        	   }
		           }
		           int price = Products.getPriceById(toBeShowed.getPid());
		           showMessages.setAlignmentX(Component.CENTER_ALIGNMENT);
		           view.removeAll();
		           view.setLayout(new BoxLayout(view, BoxLayout.Y_AXIS));
		           view.add(new JLabel("Customer name : " + toBeShowed.getCName()));
		           view.add(new JLabel("Product name : " + toBeShowed.getPName()));
		           view.add(new JLabel("Quantity : " + toBeShowed.getQuantity()));
		           view.add(new JLabel("Price(1 item): "+ price));
		           view.add(new JLabel(" "));
		           view.add(new JLabel(" "));
		           view.add(new JLabel(" "));
		           view.add(new JLabel("Total order price: "+ price * toBeShowed.getQuantity()));
		           view.add(new JLabel(" "));
		           view.add(new JLabel(" "));
		          
		                    	          
		           JButton delete = new JButton("Delete order");
		           delete.addActionListener(getDeleteOrderListener(orderId));
		           view.add(delete);
		           view.revalidate();
		           view.repaint();
		        }
		    }
		});
	}
	

	
	private ActionListener getDeleteProductListener(int productId){
		return new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				Products.deleteProduct(productId);
				view.setLayout(new BoxLayout(view, BoxLayout.Y_AXIS));
				java.util.List<Product> products = Products.getProducts();
				JTable table;
				table = ReflectionTable.retrieveTable(products);
				addProductListener(table,products);
				view.removeAll();
				if(table != null){
					view.add(table);
					view.setAlignmentX(Component.LEFT_ALIGNMENT);
				}
				else 
					view.add(new JLabel("Table is empty!"));
				insert.removeAll();
				JTextField id = new JTextField(6);
				JTextField name = new JTextField(20);
				JTextField description = new JTextField(30);
				JTextField price = new JTextField(6);
				JButton addProduct = new JButton("Insert");
				insert.add(new JLabel("Product ID :"));
				insert.add(id);
				insert.add(new JLabel("Name :"));
				insert.add(name);
				insert.add(new JLabel("Description :"));
				insert.add(description);
				insert.add(new JLabel("Price :"));
				insert.add(price);
				insert.add(addProduct);
				showMessages.removeAll();
				addProduct.addActionListener(getInsertProdListener(id,name,description,price));
				view.add(insert);
				view.add(showMessages);
				view.revalidate();
				view.repaint();
			}
		};
	}
	
	private ActionListener getDeleteOrderListener(int orderId){
		return new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
				Orders.deleteOrder(orderId);
				
				view.setLayout(new BoxLayout(view, BoxLayout.Y_AXIS));
				java.util.List<Order> orders = Orders.getOrders();
				java.util.List<Customer> cust= Customers.getCustomers();
				java.util.List<Product> prod = Products.getProducts();
				String[] customers = new String[cust.size()];
				String[] products = new String[prod.size()];
				int i = 0;
				for(Customer c : cust){
					customers[i++] = c.getId() +"." + c.getName();
				}
				i=0;
				for(Product p : prod){
					products[i++] = p.getId() + "." + p.getName();
				}
				JTable table;
				table = ReflectionTable.retrieveTable(orders);
				view.removeAll();
				if(table != null)
					view.add(table);
				else 
					view.add(new JLabel("Table is empty!"));
				insert.removeAll();
				JComboBox customerName = new JComboBox(customers);
				JComboBox productName = new JComboBox(products);
				JTextField quantity = new JTextField(6);
				
				JButton addOrder = new JButton("Make new order");
				insert.add(new JLabel("Customer Name :"));
				insert.add(customerName);
				insert.add(new JLabel(" Product Name :"));
				insert.add(productName);
				insert.add(new JLabel("Quantity :"));
				insert.add(quantity);
				quantity.setText("1");
				addOrder.addActionListener(getMakeOrderListener(customerName,productName,quantity));
				insert.add(addOrder);
				showMessages.removeAll();
				
				addOrderListener(table,orders);
				
				view.add(insert);
				view.add(showMessages);
				view.revalidate();
				view.repaint();
				
			}
		};
	}
	


	private ActionListener getInsertProdListener(JTextField id,JTextField name,JTextField description,JTextField price){
		return new ActionListener(){
			public void actionPerformed(ActionEvent e){
				 showMessages.setAlignmentX(Component.CENTER_ALIGNMENT);
				String idC = id.getText();
				String n = name.getText();
				String d = description.getText();
				String p = price.getText();
				if(idC == null || idC.isEmpty()){
					showMessages.removeAll();
					showMessages.add(new JLabel("Please insert an id!"));
					showMessages.revalidate();
					showMessages.repaint();
					return;
					
				}else if(n == null || n.isEmpty()){
					showMessages.removeAll();
					showMessages.add(new JLabel("Please insert a name!"));
					showMessages.revalidate();
					showMessages.repaint();
					return;
					
				}else if(d == null || d.isEmpty()){
					showMessages.removeAll();
					showMessages.add(new JLabel("Please insert a description!"));
					showMessages.revalidate();
					showMessages.repaint();
					return;
				}else if(p == null || p.isEmpty()){
					showMessages.removeAll();
					showMessages.add(new JLabel("Please insert a price!"));
					showMessages.revalidate();
					showMessages.repaint();	
					return;
				}
				else{
					showMessages.removeAll();
					showMessages.revalidate();
					showMessages.repaint();
				}
				Products.addNewProduct(new Integer(idC),n,d,new Integer(p));
				view.setLayout(new BoxLayout(view, BoxLayout.Y_AXIS));
				java.util.List<Product> products = Products.getProducts();
				JTable table;
				table = ReflectionTable.retrieveTable(products);
				addProductListener(table,products);
				view.removeAll();
				if(table != null){
					view.add(table);
					view.setAlignmentX(Component.LEFT_ALIGNMENT);
				}
				else 
					view.add(new JLabel("Table is empty!"));
				insert.removeAll();
				JTextField id = new JTextField(6);
				JTextField name = new JTextField(20);
				JTextField description = new JTextField(30);
				JTextField price = new JTextField(6);
				JButton addProduct = new JButton("Insert");
				insert.add(new JLabel("Product ID :"));
				insert.add(id);
				insert.add(new JLabel("Name :"));
				insert.add(name);
				insert.add(new JLabel("Description :"));
				insert.add(description);
				insert.add(new JLabel("Price :"));
				insert.add(price);
				insert.add(addProduct);
				showMessages.removeAll();
				addProduct.addActionListener(getInsertProdListener(id,name,description,price));
				view.add(insert);
				view.add(showMessages);
				view.revalidate();
				view.repaint();
				
			}
		};
	}
	
	private ActionListener getMakeOrderListener(JComboBox customerName,JComboBox productName,JTextField quantity){
		return new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String custName = customerName.getSelectedItem().toString();
				String prodName = productName.getSelectedItem().toString();
				if(quantity.getText() == null || quantity.getText().isEmpty()){
					showMessages.removeAll();
					showMessages.add(new JLabel("Please insert a quantity!"));
					showMessages.revalidate();
					showMessages.repaint();
					return;
				}
				int customerId = new Integer(custName.substring(0, custName.indexOf(".")));
				int productId = new Integer(prodName.substring(0, prodName.indexOf(".")));
				int q = new Integer(quantity.getText());
				
				int itemsAvailable = Inventorys.getQuantityById(productId); 
				if(q > itemsAvailable){
					showMessages.setAlignmentX(Component.CENTER_ALIGNMENT);
					showMessages.removeAll();
					showMessages.add(new JLabel("Only " + itemsAvailable + " left!"));
					showMessages.revalidate();
					showMessages.repaint();
					return;
				}
				int orderId = NrOrders.getNrOrders();
				Orders.insertOrder(orderId + 1,customerId,productId,q);
				
				view.setLayout(new BoxLayout(view, BoxLayout.Y_AXIS));
				java.util.List<Order> orders = Orders.getOrders();
				java.util.List<Customer> cust= Customers.getCustomers();
				java.util.List<Product> prod = Products.getProducts();
				String[] customers = new String[cust.size()];
				String[] products = new String[prod.size()];
				int i = 0;
				for(Customer c : cust){
					customers[i++] = c.getId() +"." + c.getName();
				}
				i=0;
				for(Product p : prod){
					products[i++] = p.getId() + "." + p.getName();
				}
				JTable table;
				table = ReflectionTable.retrieveTable(orders);
				view.removeAll();
				if(table != null)
					view.add(table);
				else 
					view.add(new JLabel("Table is empty!"));
				
				addOrderListener(table,orders);
				insert.removeAll();
				JComboBox customerName = new JComboBox(customers);
				JComboBox productName = new JComboBox(products);
				JTextField quantity = new JTextField(6);
				
				JButton addOrder = new JButton("Make new order");
				insert.add(new JLabel("Customer Name :"));
				insert.add(customerName);
				insert.add(new JLabel(" Product Name :"));
				insert.add(productName);
				insert.add(new JLabel("Quantity :"));
				insert.add(quantity);
				quantity.setText("1");
				addOrder.addActionListener(getMakeOrderListener(customerName,productName,quantity));
				insert.add(addOrder);
				showMessages.removeAll();
			
				view.add(insert);
				view.add(showMessages);
				view.revalidate();
				view.repaint();
			}
		};
	}

	private ActionListener getUpdateProductListener(int productId,JTextField newName,JTextField newDescription,JTextField newPrice){
		return new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String nameU = newName.getText();
				String descriptionU = newDescription.getText();
				//showMessages.setAlignmentX(Component.LEFT_ALIGNMENT);
				if(nameU == null || nameU.isEmpty()){
					showMessages.removeAll();
					showMessages.add(new JLabel("Please insert a valid name!"));
					showMessages.revalidate();
					showMessages.repaint();
					return;
					
				}else if(descriptionU == null || descriptionU.isEmpty()){
					showMessages.removeAll();
					showMessages.add(new JLabel("Please insert a valid description!"));
					showMessages.revalidate();
					showMessages.repaint();
					return;
					
				}else{
					int priceU;
					try{
						priceU = new Integer(newPrice.getText());
					}catch(Exception ex){
						showMessages.removeAll();
						showMessages.add(new JLabel("Please insert a valid price!"));
						showMessages.revalidate();
						showMessages.repaint();
						return;
					}
					
					Products.updateById(productId,nameU,descriptionU,priceU);
					
					view.setLayout(new BoxLayout(view, BoxLayout.Y_AXIS));
					java.util.List<Product> products = Products.getProducts();
					JTable table;
					table = ReflectionTable.retrieveTable(products);
					addProductListener(table,products);
					view.removeAll();
					if(table != null){
						view.add(table);
						view.setAlignmentX(Component.LEFT_ALIGNMENT);
					}
					else 
						view.add(new JLabel("Table is empty!"));
					insert.removeAll();
					JTextField id = new JTextField(6);
					JTextField name = new JTextField(20);
					JTextField description = new JTextField(30);
					JTextField price = new JTextField(6);
					JButton addProduct = new JButton("Insert");
					insert.add(new JLabel("Product ID :"));
					insert.add(id);
					insert.add(new JLabel("Name :"));
					insert.add(name);
					insert.add(new JLabel("Description :"));
					insert.add(description);
					insert.add(new JLabel("Price :"));
					insert.add(price);
					insert.add(addProduct);
					showMessages.removeAll();
					addProduct.addActionListener(getInsertProdListener(id,name,description,price));
					view.add(insert);
					view.add(showMessages);
					view.revalidate();
					view.repaint();				
				}
			}
		};
	}

	
}

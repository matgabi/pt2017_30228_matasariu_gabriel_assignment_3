package model;

public class Customer {
	
	private int customerId;
	private String name;
	private String address;
	private String email;
	
	public Customer(int customerId,String name,String address,String email){
		this.customerId = customerId;
		this.name = name;
		this.address = address;
		this.email = email;
	}
	
	public void setId(int id){
		this.customerId = id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setAddress(String address){
		this.address = address;
	}
	
	public void setEmail(String email){
		this.email = email;
	}
	
	public int getId(){
		return this.customerId;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getAddress(){
		return this.address;
	}
	
	public String getEmail(){
		return this.email;
	}
	
	public String toString(){
		return this.customerId+" " +this.name + " " + this.address + " " + this.email; 
	}
}

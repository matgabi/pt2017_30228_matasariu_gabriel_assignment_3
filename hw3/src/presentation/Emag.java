package presentation;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

import dbAccess.*;

public class Emag extends JFrame {
	
	
	
	private JPanel content = new JPanel();
	private JPanel view = new JPanel();
	private Menu menu = new Menu(view);
	
	
	public Emag(){
		initView();
	}
	
	private final void initView(){
		view.setPreferredSize(new Dimension(1360,600));
		view.setLayout(new GridLayout(1,1));
		content.add(menu);
		content.add(view);	
		this.setContentPane(content);
		this.setVisible(true);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setTitle("Queue simulator");
		this.setResizable(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	
	public static void main(String[] args){
		Emag emag = new Emag();
	}
}

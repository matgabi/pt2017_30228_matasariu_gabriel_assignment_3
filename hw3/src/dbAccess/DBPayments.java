package dbAccess;

import java.util.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Payment;
import dbConnection.ConnectionFactory;

public class DBPayments {

	private static final String getPaymentsString = "select * from payment";
	private static final String getTotalString = "select total from payment where customerId = ?";
	
	
	public static List<Payment> getPayments(){
		List<Payment> payments = new ArrayList<Payment>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement getPayments = null;
		ResultSet rs = null;
		try{
			getPayments = dbConnection.prepareStatement(getPaymentsString);
			rs = getPayments.executeQuery();
			while(rs.next()){
				int customerId = rs.getInt("customerId");
				String customerName = rs.getString("name");
				int total = rs.getInt("total");
				payments.add(new Payment(customerId,customerName,total));
			}
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(getPayments);
			ConnectionFactory.close(dbConnection);
		}
		
		return payments;
	}
	
	public static int getTotalByClientId(int clientId){
		
		int total = 0;
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement getTotal = null;
		ResultSet rs =null;
		try{
			getTotal = dbConnection.prepareStatement(getTotalString);
			getTotal.setInt(1, clientId);
			rs = getTotal.executeQuery();
			rs.next();
			total = rs.getInt("total");
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(getTotal);
			ConnectionFactory.close(dbConnection);
		}
		return total;
		
		
	}
}

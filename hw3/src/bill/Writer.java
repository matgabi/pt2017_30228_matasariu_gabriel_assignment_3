package bill;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.*;
import model.Customer;
import model.Order;

public class Writer {

	public static void createBill(Customer c,List<Order> orders,int total){
		BufferedWriter writer = null;
		try {
		    writer = new BufferedWriter(new OutputStreamWriter(
			          new FileOutputStream("bill.txt"), "utf-8"));
		    writer.write("Customer id: " + c.getId());
		    writer.newLine();
		    writer.write("Name: " + c.getName());
		    writer.newLine();
		    writer.write("Address: " + c.getAddress());
		    writer.newLine();
		    writer.write("Email: " + c.getEmail());
		    writer.newLine();
		    writer.newLine();
		    writer.newLine();
		    writer.newLine();
		    StringBuilder sb = new StringBuilder();
	    	Formatter formatter = new Formatter(sb, Locale.US);
	    	formatter.format("%1$4s %2$20s %3$10s", "Id" ,"Name", "Quantity");
		    writer.write(sb.toString());
	    	writer.newLine();
		    for(Order o : orders){
		    	sb = new StringBuilder();
		        formatter = new Formatter(sb, Locale.US);
		    	formatter.format("%1$4d %2$20s %3$10d", o.getPid() , o.getPName(), o.getQuantity());
			    writer.write(sb.toString());
		    	writer.newLine();
		    }
		    writer.newLine();
		    writer.newLine();
		    writer.newLine();
		    writer.newLine();
		    writer.write("Total: " + total);
		    
		   
		   
		} catch (IOException ex) {
		  // report
		} finally {
		   try {writer.close();} catch (Exception ex) {/*ignore*/}
		   ProcessBuilder pb = new ProcessBuilder("Notepad.exe", "bill.txt");
		   try{
			   pb.start();
		   }catch(Exception e){
			   
		   }
		}
	}
	
}

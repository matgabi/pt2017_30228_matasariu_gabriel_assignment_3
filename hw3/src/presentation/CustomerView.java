package presentation;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import bill.Writer;
import bussinessLogic.Customers;
import bussinessLogic.Orders;
import bussinessLogic.Payments;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import model.Customer;
import model.Order;
import reflection.ReflectionTable;

public class CustomerView {
	private static JPanel view;
	private static JPanel showMessages;
	private static JPanel insert;
	
	public static void setInfo(JPanel v,JPanel s,JPanel i){
		view = v;
		showMessages = s;
		insert = i;
	}
	
	public static void showCustomers(){
		view.setLayout(new BoxLayout(view, BoxLayout.Y_AXIS));
		java.util.List<Customer> customers = Customers.getCustomers();
		JTable table;
		table = ReflectionTable.retrieveTable(customers);
		addCustomerListener(table,customers);
		view.removeAll();
		if(table != null){
			view.add(table);
			view.setAlignmentX(Component.LEFT_ALIGNMENT);
		}
		else 
			view.add(new JLabel("Table is empty!"));
		insert.removeAll();
		JTextField id = new JTextField(6);
		JTextField name = new JTextField(20);
		JTextField address = new JTextField(20);
		JTextField email = new JTextField(30);
		JButton addClient = new JButton("Insert");
		insert.add(new JLabel("Customer ID :"));
		insert.add(id);
		insert.add(new JLabel("Name :"));
		insert.add(name);
		insert.add(new JLabel("Address :"));
		insert.add(address);
		insert.add(new JLabel("Email :"));
		insert.add(email);
		insert.add(addClient);
		addClient.addActionListener(getInsertListener(id,name,address,email));
		view.add(insert);
		view.add(showMessages);
		view.revalidate();
		view.repaint();
	}
	private static void addCustomerListener(JTable table,List<Customer> customers){
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
		    @Override
		    public void valueChanged(ListSelectionEvent event) {
		        if (table.getSelectedRow() > 0) {
		           int customerId = new Integer(table.getValueAt(table.getSelectedRow(), 0).toString());
		           Customer toBeShowed = null;
		           for(Customer c : customers){
		        	   if(c.getId() == customerId){
		        		   toBeShowed = c;
		        		   break;
		        	   }
		           }
		           showMessages.setAlignmentX(Component.CENTER_ALIGNMENT);
		           view.removeAll();
		           view.setLayout(new BoxLayout(view, BoxLayout.Y_AXIS));
		           view.add(new JLabel("Customer Id : " + toBeShowed.getId()+""));
		           view.add(new JLabel("Name : " + toBeShowed.getName()));
		           view.add(new JLabel("Address : " + toBeShowed.getAddress()));
		           view.add(new JLabel("E-mail : " + toBeShowed.getEmail()));
		           view.add(new JLabel(" "));
		           view.add(new JLabel(" "));
		           view.add(new JLabel("Orders: "));
		           List<Order> orders = Orders.getOrdersByClientId(customerId);
		           JTable order = ReflectionTable.retrieveTable(orders);
		           if(order != null){
		        	   order.setAlignmentX(Component.LEFT_ALIGNMENT);
		        	   view.add(order);
		           }
					else 
						view.add(new JLabel("Table is empty!"));
		           	
		           view.add(new JLabel(" "));
		           view.add(new JLabel(" "));
		           int total;
		           total = Payments.getTotalByClientId(customerId);
		           view.add(new JLabel("Total: " + total + "$"));
		           view.add(new JLabel(" "));
		           view.add(new JLabel(" "));
		           
		           JTextField newName = new JTextField(20);
		           JTextField newAddress = new JTextField(20);
		           JTextField newEmail = new JTextField(20);
		           JButton updateInfo = new JButton("Update info");
		           newName.setText(toBeShowed.getName());
		           newAddress.setText(toBeShowed.getAddress());
		           newEmail.setText(toBeShowed.getEmail());
		           
		           JPanel update = new JPanel();
		           update.add(new JLabel("New customer name: "));
		           update.add(newName);
		           update.add(new JLabel("New address: "));
		           update.add(newAddress);
		           update.add(new JLabel("New email: "));
		           update.add(newEmail);
		           update.add(updateInfo);
		           updateInfo.addActionListener(getUpdateCustomerListener(customerId,newName,newAddress,newEmail));
		           update.setAlignmentX(Component.LEFT_ALIGNMENT);
		           
		           showMessages.setAlignmentX(Component.LEFT_ALIGNMENT);
		           showMessages.removeAll();
		           view.add(update);
		           view.add(showMessages);
		           
		           JButton delete = new JButton("Delete customer");
		           delete.addActionListener(getDeleteListener(customerId));
		           JButton bill = new JButton("Create bill");
		           bill.addActionListener(getBillListener(toBeShowed,orders,total));
		           view.add(delete);
		           view.add(bill);
		           view.revalidate();
		           view.repaint();
		        }
		    }
		});
	}

	private static  ActionListener getInsertListener(JTextField id,JTextField name,JTextField addr,JTextField email){
		return new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String idC = id.getText();
				String n = name.getText();
				String ad = addr.getText();
				String em = email.getText();
				 showMessages.setAlignmentX(Component.CENTER_ALIGNMENT);
				if(idC == null || idC.isEmpty()){
					showMessages.removeAll();
					showMessages.add(new JLabel("Please insert an id!"));
					showMessages.revalidate();
					showMessages.repaint();
					return;
					
				}else if(n == null || n.isEmpty()){
					showMessages.removeAll();
					showMessages.add(new JLabel("Please insert a name!"));
					showMessages.revalidate();
					showMessages.repaint();
					return;
					
				}else if(ad == null || ad.isEmpty()){
					showMessages.removeAll();
					showMessages.add(new JLabel("Please insert an address!"));
					showMessages.revalidate();
					showMessages.repaint();
					return;
				}else if(em == null || em.isEmpty()){
					showMessages.removeAll();
					showMessages.add(new JLabel("Please insert an email!"));
					showMessages.revalidate();
					showMessages.repaint();	
					return;
				}
				else{
					showMessages.removeAll();
					showMessages.revalidate();
					showMessages.repaint();
				}
				Customers.addNewCustomer(new Integer(idC),n,ad,em);
				showCustomers();
			}
		};
	}
	private static ActionListener getDeleteListener(int customerId){
		return new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				
				Customers.deleteCustomer(customerId);
				
				showCustomers();
			}
		};
	}
	
	private static ActionListener getUpdateCustomerListener(int customerId,JTextField newName,JTextField newAddress,JTextField newEmail){
		return new ActionListener(){
			public void actionPerformed(ActionEvent e){
				String nameU = newName.getText();
				String addressU = newAddress.getText();
				String emailU = newEmail.getText();
				//showMessages.setAlignmentX(Component.LEFT_ALIGNMENT);
				if(nameU == null || nameU.isEmpty()){
					showMessages.removeAll();
					showMessages.add(new JLabel("Please insert a valid name!"));
					showMessages.revalidate();
					showMessages.repaint();
					return;
					
				}else if(addressU == null || addressU.isEmpty()){
					showMessages.removeAll();
					showMessages.add(new JLabel("Please insert a valid address!"));
					showMessages.revalidate();
					showMessages.repaint();
					return;
					
				}else if(emailU == null || emailU.isEmpty()){
					showMessages.removeAll();
					showMessages.add(new JLabel("Please insert a valid email!"));
					showMessages.revalidate();
					showMessages.repaint();
					return;
				}else{
						
					Customers.updateById(customerId,nameU,addressU,emailU);
					showCustomers();
				}
			}
		};
	}
	
	private static ActionListener getBillListener(Customer c,List<Order> orders,int total){
		return new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Writer.createBill(c, orders,total);
			}
		};
	}

}

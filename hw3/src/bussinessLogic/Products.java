package bussinessLogic;
import model.Product;
import dbAccess.DBCustomers;
import dbAccess.DBProducts;
import java.util.*;


public class Products {
	
	public static List<Product> getProducts(){
		return DBProducts.getProducts();
	}
	
	public static void addNewProduct(int idProduct,String name,String description,int price){
		DBProducts.addNewProduct(idProduct, name, description, price);
	}
	
	public static void deleteProduct(int productId){
		DBProducts.deleteProduct(productId);
	}
	
	public static int getPriceById(int priceId){
		return DBProducts.getPriceById(priceId);
	}
	
	public static void updateById(int productId,String name,String description,int price){
		DBProducts.updateById(productId,name,description,price);
	}
	
}

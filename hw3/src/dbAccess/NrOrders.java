package dbAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbConnection.ConnectionFactory;

public class NrOrders {
	private static final String getNrOrdersString = "select * from nrorders";
	public static int getNrOrders(){
		
		String nrOrders = null;
		
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement getNrStatement = null;
		ResultSet rs = null;
		try{
			getNrStatement = dbConnection.prepareStatement(getNrOrdersString);
			rs = getNrStatement.executeQuery();
			rs.next();
			nrOrders = rs.getString("nrorders");
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(getNrStatement);
			ConnectionFactory.close(dbConnection);
		}
		return new Integer(nrOrders);
	}
}

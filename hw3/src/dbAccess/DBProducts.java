package dbAccess;
import java.util.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Product;
import dbConnection.ConnectionFactory;

public class DBProducts {
	
	private static final String getProductsString = "select * from products";
	private static final String addNewProductString = "insert into products values(?,?,?,?)";
	private static final String deleteProductString = "delete from products where productId = ?";
	private static final String getPriceByIdString = "select price from products where productId = ?";
	private static final String updateByIdString = "update products set name= ?, description = ? , price = ? where productId= ?";
	
	
	public static List<Product> getProducts(){
		List<Product> products = new ArrayList<Product>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement getProducts = null;
		ResultSet rs = null;
		try{
			getProducts = dbConnection.prepareStatement(getProductsString);
			rs = getProducts.executeQuery();
			while(rs.next()){
				int id = rs.getInt("productId");
				String name = rs.getString("name");
				String description = rs.getString("description");
				int price = rs.getInt("price");
				products.add(new Product(id,name,description,price));
			}
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(getProducts);
			ConnectionFactory.close(dbConnection);
		}
		return products;
	}
	
	public static void addNewProduct(int idProduct,String name,String description,int price){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement addNewProduct = null;
		try{
			addNewProduct = dbConnection.prepareStatement(addNewProductString);
			addNewProduct.setInt(1, idProduct);
			addNewProduct.setString(2, name);
			addNewProduct.setString(3, description);
			addNewProduct.setInt(4, price);
			addNewProduct.executeUpdate();
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(addNewProduct);
			ConnectionFactory.close(dbConnection);
		}
	}

	public static void deleteProduct(int productId){

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteProduct = null;
		int rs;
		try{
			deleteProduct = dbConnection.prepareStatement(deleteProductString);
			deleteProduct.setInt(1, productId);
			rs = deleteProduct.executeUpdate();
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(deleteProduct);
			ConnectionFactory.close(dbConnection);
		}
		
	}

	public static int getPriceById(int productId){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement getPriceById = null;
		ResultSet rs = null;
		int price = 0;
		try{
			getPriceById = dbConnection.prepareStatement(getPriceByIdString);
			getPriceById.setInt(1, productId);
			rs = getPriceById.executeQuery();
			
			rs.next();
			price = rs.getInt("price");
			
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(getPriceById);
			ConnectionFactory.close(dbConnection);
		}
		return price;
	}

	public static void updateById(int productId,String name,String description,int price){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateProduct = null;
		try{
			updateProduct = dbConnection.prepareStatement(updateByIdString);
			updateProduct.setInt(4, productId);
			updateProduct.setString(1, name);
			updateProduct.setString(2, description);
			updateProduct.setInt(3, price);
			updateProduct.executeUpdate();
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(updateProduct);
			ConnectionFactory.close(dbConnection);
		}
	}

}

package bussinessLogic;
import dbAccess.DBCustomers;
import model.Customer;
import java.util.*;

public class Customers {
	
	public static List<Customer> getCustomers(){
		return DBCustomers.getCustomers();
	}
	
	public static void deleteCustomer(int customerId){
		DBCustomers.deleteCustomer(customerId);
	}
	
	public static void addNewCustomer(int idCustomer,String name,String address,String email){
		DBCustomers.addNewCustomer(idCustomer,name,address,email);
	}
	
	public static void updateById(int idCustomer,String name,String address,String email){
		DBCustomers.updateById(idCustomer,name,address,email);
	}
	
}

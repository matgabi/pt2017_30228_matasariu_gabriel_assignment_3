package presentation;

import java.awt.*;
import javax.swing.*;


public class Menu extends JPanel {
	private JButton customers = new JButton("customers");
	private JButton products = new JButton("products");
	private JButton orders = new JButton("orders");
	private JButton inventory = new JButton("inventory");
	private JButton payments = new JButton("payments");
	
	private JPanel view;
	private Listeners listeners;
	
	public Menu(JPanel view){
		this.view = view;
		listeners = new Listeners(view);
		customers.addActionListener(listeners.getCustomerListener());
		products.addActionListener(listeners.getProductListener());
		inventory.addActionListener(listeners.getInventoryListener());
		orders.addActionListener(listeners.getOrdersListener());
		payments.addActionListener(listeners.getPaymentsListener());
		
		
		this.setLayout(new GridLayout(1,5));
		this.add(customers);
		this.add(products);
		this.add(orders);
		this.add(inventory);
		this.add(payments);
		this.setPreferredSize(new Dimension(1360,50));
	}
}

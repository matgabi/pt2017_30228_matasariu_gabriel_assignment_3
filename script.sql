
create table customer(
	customerId int,
	name varchar(20),
	address varchar(20),
	email varchar(20)
);

create table products(
	productId int,
	name varchar(20),
	description varchar(30),
	price int
);

create table orders(
	orderId int,
	customerId int,
	productId int,
	quantity int
);

create table inventory(
	productId int,
	quantity int
);

create table payment(
	customerId int,
	name varchar(20),
	total int
);

create table nrorders(
	nrorders int default 0
);

alter table customer add primary key(customerId);
alter table products add primary key(productId);
alter table orders add primary key(orderId);
alter table inventory add primary key(productId);
alter table payment add primary key(customerId);
alter table nrorders add primary key(nrorders);

alter table inventory add foreign key(productId) references products(productId) on delete cascade;
alter table payment add foreign key(customerId) references customer(customerId) on delete cascade;
alter table orders add foreign key(customerId) references customer(customerId) on delete cascade;
alter table orders add foreign key(productId) references products(productId) on delete cascade;


delimiter //
create trigger add_payment after insert on customer
for each row
begin
	insert into payment values(new.customerId,new.name,0);
end //
delimiter ;


delimiter //
create trigger update_name after update on customer
for each row
begin
	update payment set name = new.name where customerId = new.customerId;  
end //
delimiter ;

delimiter //
create trigger add_price after insert on orders
for each row
begin
	declare p int;
	(select price into p from products where productId = new.productId);
	update payment set total = total + p * new.quantity where customerId=new.customerId;
end //
delimiter ;

delimiter //
create trigger delete_price after delete on orders
for each row
begin
	declare p int;
	(select price into p from products where productId = old.productId);
	update payment set total = total - p * old.quantity where customerId=old.customerId;
end //
delimiter ;

delimiter //
create trigger add_inventory after insert on products
for each row
begin
	insert into inventory values(new.productId,new.name,5);
end //
delimiter ;

delimiter //
create trigger update_nr after insert on orders
for each row
begin
	update nrorders set nrorders = nrorders + 1;
end //
delimiter ;

insert into customer values(1,'Gabi','Cluj','mat.gabi@yahoo.com');
insert into customer values(2,'Codri','Iaso','codruts33@gmail.com');
insert into customer values(3,'Tudor','Neamt','tudorb@yahoo.com');

insert into products values(100,'iPhone 5se','64GB/128GB',349);
insert into products values(101,'iPhone 5','16GB/128GB',149);
insert into products values(102,'iPhone 6','64GB',449);
insert into products values(103,'iPhone 8','64GB/128GB',749);

insert into orders values(1,1,100,2);
insert into orders values(2,1,103,1);
insert into orders values(3,2,102,4);
insert into orders values(4,3,101,3);
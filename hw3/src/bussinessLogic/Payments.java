package bussinessLogic;

import java.util.*;
import model.Payment;
import dbAccess.DBPayments;

public class Payments {

	public static List<Payment> getPayments(){
		return DBPayments.getPayments();
	}
	
	public static int getTotalByClientId(int clientId){
		return DBPayments.getTotalByClientId(clientId);
	}
}

package dbAccess;

import java.util.*;

import dbConnection.ConnectionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Inventory;
import dbConnection.ConnectionFactory;

public class DBInventory {

	private static final String getInventoryString = "select productId,name,quantity from inventory natural join products";
	private static final String getQuantityString = "select quantity from inventory where productId = ?";
	
	public static List<Inventory> getInventory(){
		List<Inventory> inventory = new ArrayList<Inventory>();
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement getInventory = null;
		ResultSet rs = null;
		
		try{
			getInventory = dbConnection.prepareStatement(getInventoryString);
			rs = getInventory.executeQuery();
			while(rs.next()){
				int productId = rs.getInt("productId");
				String name = rs.getString("name");
				int quantity = rs.getInt("quantity");
				inventory.add(new Inventory(productId,name,quantity));
			}
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(getInventory);
			ConnectionFactory.close(dbConnection);
		}
		
		return inventory;
	}
	
	public static int getQuantityById(int productId){
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement getQuantity = null;
		ResultSet rs = null;
		int quantity= 0;
		try{
			getQuantity = dbConnection.prepareStatement(getQuantityString);
			getQuantity.setInt(1, productId);
			rs = getQuantity.executeQuery();
			rs.next();
			
			quantity = rs.getInt("quantity");
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(getQuantity);
			ConnectionFactory.close(dbConnection);
		}
		return quantity;
	}
}

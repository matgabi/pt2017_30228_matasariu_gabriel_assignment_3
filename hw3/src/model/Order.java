package model;

public class Order {

	private int orderId;
	private int customerId;
	private String nameCustomer;
	private int productId;
	private String nameProduct;
	private int quantity;
	
	public Order(int orderId,int customerId,String nameCustomer,int productId, String productName,int quantity){
		this.orderId = orderId;
		this.customerId = customerId;
		this.nameCustomer = nameCustomer;
		this.productId = productId;
		this.nameProduct = productName;
		this.quantity = quantity;
	}
	
	public void setOId(int id){
		this.orderId = id;
	}
	
	public void setCId(int id){
		this.customerId = id;
	}
	
	public void setCName(String name){
		this.nameCustomer = name;
	}
	
	public void setPId(int id){
		this.productId = id;
	}
	
	public void setPName(String name){
		this.nameProduct = name;
	}
	
	public void setQuantity(int quantity){
		this.quantity = quantity;
	}
	
	public int getOId(){
		return this.orderId;
	}
	
	public int getCId(){
		return this.customerId;
	}
	
	public String getCName(){
		return this.nameCustomer;
	}
	
	public int getPid(){
		return this.productId;
	}
	
	public String getPName(){
		return this.nameProduct;
	}
	
	public int getQuantity(){
		return this.quantity;
	}
	
	public String toString(){
		return this.orderId + " " + this.customerId + " " + this.nameCustomer+ " " + this.productId + " "+
									this.nameProduct + " "+ this.quantity;
	}
}

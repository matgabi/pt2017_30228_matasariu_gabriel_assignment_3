package bussinessLogic;

import java.util.*;
import model.Order;
import dbAccess.DBOrders;

public class Orders {

	public static List<Order> getOrders(){
		return DBOrders.getOrders();
	}
	
	public static List<Order> getOrdersByClientId(int customerId){
		return DBOrders.getOrdersByClientId(customerId);
	}
	
	public static void insertOrder(int orderId,int customerId,int productId,int quantity){
		DBOrders.insertOrder(orderId,customerId,productId,quantity);
	}
	
	public static void deleteOrder(int orderId){
		DBOrders.deleteOrder(orderId);
	}
}

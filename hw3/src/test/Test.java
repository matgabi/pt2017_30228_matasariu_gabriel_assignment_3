package test;
import dbAccess.*;
import model.*;
import java.util.*;

public class Test {
	public static void main(String[] args){
		System.out.println(NrOrders.getNrOrders());
		List<Customer> customers = DBCustomers.getCustomers();
		for(Customer c : customers)
			System.out.println(c);
		List<Product> products = DBProducts.getProducts();
		for(Product p : products){
			System.out.println(p);
		}
		List<Inventory> inventory = DBInventory.getInventory();
		for(Inventory i : inventory){
			System.out.println(i);
		}
		
		List<Order> orders = DBOrders.getOrders();
		for(Order o : orders){
			System.out.println(o);
		}
		
		List<Payment> payments = DBPayments.getPayments();
		for(Payment p : payments){
			System.out.println(p);
		}
	}
}

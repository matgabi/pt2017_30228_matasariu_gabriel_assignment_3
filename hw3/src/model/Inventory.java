package model;

public class Inventory {

	private int productId;
	private String name;
	private int quantity;
	
	public Inventory(int id, String name, int quantity){
		this.productId = id;
		this.name = name;
		this.quantity = quantity;
	}
	
	public void setPId(int id){
		this.productId = id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setQuantity(int quantity){
		this.quantity = quantity;
	}
	
	public int getPid(){
		return this.productId;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getQuantity(){
		return this.quantity;
	}
	
	public String toString(){
		return this.productId + " " + this.name + " " + this.quantity;
	}
	
}
